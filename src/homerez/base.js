'use strict';

global._        = require('lodash');
global.HR       = require('homerez-constants');
global.log4js   = require('log4js');
global.mongoose = require('mongoose');
global.Q        = require('q');
global.rek      = require('rekuire');
global.sprintf  = require('sprintf-js').sprintf;
